# README #

This is a learning project application utilizing:
* Modern C++ (C++ 11)
* Google Test and Google Mock
* VSCode for an IDE:
 * Extensions:
  * GoogleTest Adapter
  * Coverage Gutters
  * Microsoft CPP Support (CPPTools, C/C++)
 * My Extensions:
  * GitLens
  * Peacock
  * Live Share

# The Project #

Build a virtual file system utilizing things such as Test Driven Development, the agreed upcon coding guidelines, and so forth.
