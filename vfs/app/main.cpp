/**
 * @file main.cpp
 * @brief virtual file system application main file.
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#include <iostream>
#include <memory>

#include "interfaces/FileSystemComponentInterface.h"
#include "interfaces/FileSystemInterface.h"
#include "FileSystem.h"
#include "CommandParser.h"

/**
 * @brief Entry function for virtual file system application.
 * 
 * @return int : 0 for succes, else with error
 */
int main() {
    const char* APPLICATION_GREETING = "Welcome to the Virtual File System application.";
    const char* APPLICATION_EXIT = "Press ctrl+c to exit....";
    const char* APPLICATION_PROMOT = "vfs> ";

    std::cout << APPLICATION_GREETING << std::endl;
    std::cout << APPLICATION_EXIT << std::endl;

    std::shared_ptr<FileSystemInterface> fileSystemInstance = FileSystem::getInstance();
    std::unique_ptr<CommandParserInterface> commandParser(new CommandParser(fileSystemInstance));

    std::string command;
    while(true)
    {
        std::cout << APPLICATION_PROMOT;
        getline(std::cin, command);

        auto output = commandParser->excuteCommand(command);

        for (auto &str : output)
        {
            std::cout << "  " << str << std::endl;
        }
    }

    return 0;
}
