#ifndef VFS_COMMANDINVALID_H
#define VFS_COMMANDINVALID_H


#include <vector>
#include <string>
#include <memory>

#include "interfaces/CommandInterface.h"
#include "interfaces/FileSystemComponentInterface.h"
#include "interfaces/FileSystemInterface.h"

/**
 * @brief Class for Invalid/Not-Supported Commands
 * 
 */
class CommandInvalid : public CommandInterface
{
    public:
        std::vector<std::string> execute(std::vector<std::string> commandArgs,
                    std::shared_ptr<FileSystemComponentInterface> fileSysComp,
                    std::shared_ptr<FileSystemInterface> fileSystem);
        bool isValidArgs(std::vector <std::string> commandArgs,
                    std::shared_ptr<FileSystemComponentInterface> fileSysComp,
                    std::shared_ptr<FileSystemInterface> fileSystem);
};

#endif