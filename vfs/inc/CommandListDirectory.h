#ifndef VFS_COMMANDLISTDIRECTORY_H
#define VFS_COMMANDLISTDIRECTORY_H

#include <vector>
#include <string>
#include <memory>

#include "interfaces/CommandInterface.h"
#include "interfaces/FileSystemComponentInterface.h"
#include "interfaces/FileSystemInterface.h"

/**
 * @brief Class for listing directory content command.
 * 
 */
class CommandListDirectory : public CommandInterface
{
    public:
        std::vector<std::string> execute(std::vector<std::string> commandArgs,
                    std::shared_ptr<FileSystemComponentInterface> fileSysComp,
                    std::shared_ptr<FileSystemInterface> fileSystem);
        bool isValidArgs(std::vector <std::string> commandArgs,
                    std::shared_ptr<FileSystemComponentInterface> fileSysComp,
                    std::shared_ptr<FileSystemInterface> fileSystem);
};

#endif