#ifndef VFS_COMMANDMAKEDIRECTORY_H
#define VFS_COMMANDMAKEDIRECTORY_H

#include <vector>
#include <string>
#include <memory>

#include "interfaces/CommandInterface.h"
#include "interfaces/FileSystemComponentInterface.h"
#include "interfaces/FileSystemInterface.h"


/**
 * @brief Class for creating new directory Command.
 * 
 */
class CommandMakeDirectory : public CommandInterface
{
    public:
        std::vector<std::string> execute(std::vector<std::string> commandArgs,
                    std::shared_ptr<FileSystemComponentInterface> fileSysComp,
                    std::shared_ptr<FileSystemInterface> fileSystem);
        bool isValidArgs(std::vector <std::string> commandArgs,
                    std::shared_ptr<FileSystemComponentInterface> fileSysComp,
                    std::shared_ptr<FileSystemInterface> fileSystem);
};

#endif