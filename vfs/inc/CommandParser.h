#ifndef VFS_COMMANDPARSER_H
#define VFS_COMMANDPARSER_H

#include <string>
#include <vector>
#include <memory>

#include "interfaces/CommandParserInterface.h"
#include "interfaces/CommandInterface.h"
#include "interfaces/FileSystemComponentInterface.h"
#include "interfaces/FileSystemInterface.h"

#include "types/CommandType.h"


/**
 * @brief Class for parsing and executing command.
 * 
 */
class CommandParser : public CommandParserInterface
{
    public:
        CommandParser(std::shared_ptr<FileSystemInterface> fileSystem);
        ~CommandParser();
        std::vector<std::string> excuteCommand(std::string command);
        std::vector<std::string> getAllCommands();

    private:
        std::shared_ptr<FileSystemInterface> fileSystem_;
        std::shared_ptr<FileSystemComponentInterface> currentDirectory_;
        std::vector<std::string> currentCommand_;

        CommandType getCommand();
        std::vector<std::string> getCommandArgs();
        std::shared_ptr<CommandInterface> getCommandObject(CommandType command);
        std::vector<std::string> tokenizeCommand(std::string command);

};

#endif