#ifndef VFS_FILECOMPONENTHASHER_H
#define VFS_FILECOMPONENTHASHER_H

/**
 * @brief Class to generate Hash for file system component
 * unordered map
 * 
 */
class FileComponentHasher
{
    public:
        size_t operator() (const FileComponentKey &key)const
        {
            return sizeof(key.name_) + key.isDirectory_ ? 1 : 0;
        }
};

#endif