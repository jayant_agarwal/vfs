#ifndef VFS_FILECOMPONENTKEY_H
#define VFS_FILECOMPONENTKEY_H

#include <string>

/**
 * @brief Structure to store key for file system component
 * unordered map.
 */
struct FileComponentKey
{
    public:
        std::string name_;
        bool isDirectory_;

        FileComponentKey(std::string name, bool isDirectory)
        {
            name_ = name;
            isDirectory_ = isDirectory;
        }

        bool operator == (const FileComponentKey &other) const
        {
            return (name_ == other.name_) && (isDirectory_ == other.isDirectory_);
        }
};

#endif