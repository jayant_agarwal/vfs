#ifndef VFS_FILESYSTEM_H
#define VFS_FILESYSTEM_H

#include <memory>
#include <string>
#include <vector>

#include "interfaces/FileSystemInterface.h"
#include "interfaces/FileSystemComponentInterface.h"

/**
 * @brief Class for File system
 * 
 */
class FileSystem : public FileSystemInterface
{
    private:
        static std::shared_ptr<FileSystem> instance_;
	    std::shared_ptr<FileSystemComponentInterface> rootDirectory_;
    
    public :
        FileSystem();	
        ~FileSystem(); 	
        std::shared_ptr<FileSystemComponentInterface>  getRootDirectory();
        static std::shared_ptr<FileSystem> getInstance();
        bool createDirectory(std::string directoryName, std::shared_ptr<FileSystemComponentInterface> parent);
        std::vector<std::string> listDirectory(std::shared_ptr<FileSystemComponentInterface> parent);
};

#endif