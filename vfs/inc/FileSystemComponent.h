#ifndef VFS_FILESYSTEMCOMPONENT_H
#define VFS_FILESYSTEMCOMPONENT_H

#include <string>
#include <memory>
#include <vector>
#include <cinttypes>
#include <unordered_map>

#include "interfaces/FileSystemComponentInterface.h"

#include "FileComponentKey.h"
#include "FileComponentHasher.h"

/**
 * @brief Class for File system component
 * 
 */
class FileSystemComponent : public FileSystemComponentInterface
{
    private:
        std::string name_;
        int size_;
        uint16_t flags_;
        std::unordered_map<FileComponentKey, std::shared_ptr<FileSystemComponentInterface>, FileComponentHasher> subComponents_;

	public :
        FileSystemComponent();
        ~FileSystemComponent();
        std::unique_ptr<FileSystemComponent>& operator=(
                            std::unique_ptr<FileSystemComponent>& ) = delete;
        std::string getName();
        void setName(std::string name);
        uint16_t getFlags();
        void setFlags(uint16_t flags);
        int getSize();
        void setSize(int size);
        bool insertChildNode(std::shared_ptr<FileSystemComponentInterface> child);
        std::vector<std::string> listChildNodes();
};

#endif