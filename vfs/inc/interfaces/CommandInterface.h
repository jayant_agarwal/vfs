#ifndef VFS_COMMANDINTERFACE_H
#define VFS_COMMANDINTERFACE_H

#include <vector>
#include <string>
#include <memory>

#include "interfaces/FileSystemComponentInterface.h"
#include "interfaces/FileSystemInterface.h"

/**
 * @brief Interface for commands.
 * 
 */
class CommandInterface
{
    public:
        virtual std::vector<std::string> execute(std::vector <std::string> commandArgs, std::shared_ptr<FileSystemComponentInterface> fileSysComp, std::shared_ptr<FileSystemInterface> fileSystem) = 0;
        virtual bool isValidArgs(std::vector <std::string> commandArgs, std::shared_ptr<FileSystemComponentInterface> fileSysComp, std::shared_ptr<FileSystemInterface> fileSystem) = 0;
};

#endif