#ifndef VFS_COMMANDPARSERINTERFACE_H
#define VFS_COMMANDPARSERINTERFACE_H

#include <string>
#include <vector>

/**
 * @brief Interface for command parser
 * 
 */
class CommandParserInterface
{
    public:
        virtual std::vector<std::string> excuteCommand(std::string command) = 0;
};

#endif