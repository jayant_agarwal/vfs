#ifndef VFS_FILESYSTEMCOMPONENTINTERFACE_H
#define VFS_FILESYSTEMCOMPONENTINTERFACE_H


#include <string>
#include <memory>
#include <vector>
#include <cinttypes>


/**
 * @brief File System Component Interface
 * 
 */
class FileSystemComponentInterface
{
    public:
        virtual void setSize(int size) = 0;
        virtual int getSize() = 0;
        virtual uint16_t getFlags() = 0;
        virtual void setFlags(uint16_t flags) = 0;
        virtual void setName(std::string name) = 0;
        virtual std::string getName() = 0;
        virtual bool insertChildNode(std::shared_ptr<FileSystemComponentInterface>) = 0;
        virtual std::vector<std::string> listChildNodes() = 0;
};

#endif