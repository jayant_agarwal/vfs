#ifndef VFS_FILESYSTEMINTERFACE_H
#define VFS_FILESYSTEMINTERFACE_H

#include <memory>
#include <string>
#include <memory>

#include "interfaces/FileSystemComponentInterface.h"

/**
 * @brief File System Interface
 * 
 */
class FileSystemInterface
{
    public:	
	    virtual std::shared_ptr<FileSystemComponentInterface>  getRootDirectory() = 0;
        virtual bool createDirectory(std::string directoryName, std::shared_ptr<FileSystemComponentInterface> parent) = 0;
        virtual std::vector<std::string> listDirectory(std::shared_ptr<FileSystemComponentInterface> parent) = 0;
};

#endif