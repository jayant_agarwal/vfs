#ifndef VFS_COMMANDTYPE_H
#define VFS_COMMANDTYPE_H

/**
 * @brief Enum for Command types
 * 
 */
enum class CommandType
{
    INVALID,
    NO_COMMAND,
    CREATE_DIR,
    LIST_DIR
};

#endif