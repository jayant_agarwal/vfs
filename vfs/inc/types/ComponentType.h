#ifndef VFS_COMPONENTTYPE_H
#define VFS_COMPONENTTYPE_H

/**
 * @brief Enum for File System Component Types
 * 
 */
enum class ComponentType
{
    INVALID,
    FILE,
    DIRECTORY,
};

#endif