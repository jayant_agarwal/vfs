#ifndef VFS_FILETYPE_H
#define VFS_FILETYPE_H

/**
 * @brief Enum for File Types
 * 
 */
enum class FileType
{
    INVALID,
    TEXT,
    BINARY,
};

#endif