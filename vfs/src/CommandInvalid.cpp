/**
 * @file CommandInvalid.cpp
 * @brief 
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#include <iostream>

#include "interfaces/FileSystemComponentInterface.h"
#include "CommandInvalid.h"
#include "interfaces/FileSystemInterface.h"

/**
 * @brief Function handles invalid or not-supported commands
 * 
 * @param commandArgs Arguments provided for this command
 * @param fileSysComp File system component at which level command executed
 * @param fileSystem File system object
 * @return vector<string> strings to be displayed to user on execution
 */
std::vector<std::string> CommandInvalid::execute(std::vector<std::string> commandArgs, std::shared_ptr<FileSystemComponentInterface> fileSysComp,
std::shared_ptr<FileSystemInterface> fileSystem)
{
    std::vector<std::string> output{"[Error]: There is no such command available"};
    return output;
}


/**
 * @brief Validates all the arguments provided to command. 
 * 
 * @param commandArgs Arguments provided for this command
 * @param fileSysComp File system component at which level command executed
 * @param fileSystem File system object
 * @return true if arguments are valid
 * @return false if arguments are not valid
 */
bool CommandInvalid::isValidArgs(std::vector<std::string> commandArgs, std::shared_ptr<FileSystemComponentInterface> fileSysComp,
std::shared_ptr<FileSystemInterface> fileSystem)
{
    return true;
}