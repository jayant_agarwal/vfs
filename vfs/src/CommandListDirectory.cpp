/**
 * @file CommandListDirectory.cpp
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2020-04-02
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#include <iostream>
#include <sstream>

#include "interfaces/FileSystemComponentInterface.h"
#include "CommandListDirectory.h"
#include "interfaces/FileSystemInterface.h"

/**
 * @brief Function handles list directory commands
 * 
 * @param commandArgs Arguments provided for this command
 * @param fileSysComp File system component at which level command executed
 * @param fileSystem File system object
 * @return vector<string> strings to be displayed to user on execution
 */
std::vector<std::string> CommandListDirectory::execute(std::vector<std::string> commandArgs, std::shared_ptr<FileSystemComponentInterface> fileSysComp,
                                    std::shared_ptr<FileSystemInterface> fileSystem)
{
    return fileSystem->listDirectory(fileSysComp);
}


/**
 * @brief Validates all the arguments provided to command. 
 * 
 * @param commandArgs Arguments provided for this command
 * @param fileSysComp File system component at which level command executed
 * @param fileSystem File system object
 * @return true if arguments are valid
 * @return false if arguments are not valid
 */
bool CommandListDirectory::isValidArgs(std::vector<std::string> commandArgs, std::shared_ptr<FileSystemComponentInterface> fileSysComp,
std::shared_ptr<FileSystemInterface> fileSystem)
{
    return (commandArgs.size() == 0);
}