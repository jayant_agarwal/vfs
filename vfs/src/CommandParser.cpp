/**
 * @file CommandParser.cpp
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2020-04-02
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#include <iostream>
#include <memory>
#include <sstream>

#include "interfaces/CommandInterface.h"
#include "interfaces/FileSystemInterface.h"

#include "types/CommandType.h"

#include "CommandParser.h"

#include "CommandMakeDirectory.h"
#include "CommandListDirectory.h"
#include "CommandInvalid.h"

/**
 * @brief Construct a new Command Parser:: Command Parser object
 * 
 * @param fileSystem 
 */
CommandParser::CommandParser(std::shared_ptr<FileSystemInterface> fileSystem)
{
    /* fill fileSystem_ member */
    fileSystem_ = fileSystem;

    /* get root directory and store in currentDirectory_ member */
    if (fileSystem_)
    {
        currentDirectory_ = fileSystem_->getRootDirectory();
    }
}


/**
 * @brief Destroy the Command Parser:: Command Parser object
 * 
 */
CommandParser::~CommandParser()
{
}

/**
 * @brief Function takes command input string and excutes the command
 * 
 * @param command user input command string
 * @return vector<string> response to the user given command
 */
std::vector<std::string> CommandParser::excuteCommand(std::string command)
{
    currentCommand_ = tokenizeCommand(command);
    std::vector<std::string> ouput;

    std::shared_ptr<CommandInterface> cmd = getCommandObject(getCommand());
    if (cmd) {
        std::vector<std::string> args = getCommandArgs();
        if(cmd->isValidArgs(args, currentDirectory_, fileSystem_))
        {
            ouput = cmd->execute(args, currentDirectory_, fileSystem_);
        }
        else
        {
            ouput.push_back("[Error]: Please provide valid arguments to command.");
        }
    } 
    return ouput;
}


/**
 * @brief Function maps command string to command type enum value.
 * 
 * @return CommandType command type enum value
 */
CommandType CommandParser::getCommand()
{
    CommandType cmdType = CommandType::INVALID;
    if (currentCommand_[0] == "mkdir")
    {
        cmdType = CommandType::CREATE_DIR;
    } 
    else if (currentCommand_[0] == "lsdir")
    {
        cmdType = CommandType::LIST_DIR;
    } 
    else if (currentCommand_[0] == "no_command")
    {
        cmdType = CommandType::NO_COMMAND;
    }

    return cmdType;
}


/**
 * @brief Function uses command vector stored in class and 
 * return argument list for command
 * 
 * @return vector<string> argument list for command
 */
std::vector<std::string> CommandParser::getCommandArgs()
{
    std::vector<std::string> args(currentCommand_.begin() + 1, currentCommand_.end());
    return args;
}


/**
 * @brief Function gets particular command object(child class object) 
 * based on command type.
 * 
 * @param command Command type enum value
 * @return std::shared_ptr<CommandInterface> :command object(child class object)
 */
std::shared_ptr<CommandInterface> CommandParser::getCommandObject(CommandType command)
{
    switch(command)
    {
        case CommandType::CREATE_DIR :
            return std::shared_ptr<CommandInterface>(new CommandMakeDirectory());    
        case CommandType::LIST_DIR :
            return std::shared_ptr<CommandInterface>(new CommandListDirectory());    
        case CommandType::NO_COMMAND :
            return nullptr;
        default:
            return std::shared_ptr<CommandInterface>(new CommandInvalid());    
    }
}


/**
 * @brief This function tokenize the string command based on space.
 * 
 * @param command : string command input from user.
 * @return vector<string> tokenized string vector
 */
std::vector<std::string> CommandParser::tokenizeCommand(std::string command)
{
    std::vector<std::string> commandTokens;

    std::stringstream ss(command);
    std::string token;

    while(getline(ss, token, ' '))
    {
        if (token.size() >= 1)
        {
            commandTokens.push_back(token);
        }
    }

    if (commandTokens.size() == 0)
    {
        commandTokens.push_back("no_command");
    }

    return commandTokens;
}