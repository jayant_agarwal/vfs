/**
 * @file FileSystem.cpp
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2020-04-02
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#include <memory>
#include <string>
#include <vector>

#include "FileSystem.h"
#include "FileSystemComponent.h"

/**
 * @brief File system object to make fileSystem class sigelton.
 * 
 */
std::shared_ptr<FileSystem> FileSystem::instance_ = nullptr;

/**
 * @brief Construct a new File System:: File System object
 * 
 */
FileSystem::FileSystem() 
{
   rootDirectory_ = std::shared_ptr<FileSystemComponent>(new FileSystemComponent());
   rootDirectory_->setName("/");
   rootDirectory_->setSize(0);  
   rootDirectory_->setFlags(1); 
}

/**
 * @brief Function fetch root directory for this file system
 * 
 * @return std::shared_ptr<FileSystemComponent>  root directory of file system
 */
std::shared_ptr<FileSystemComponentInterface>  FileSystem::getRootDirectory()
{

    return rootDirectory_;
}

/**
 * @brief Destroy the File System:: File System object
 * 
 */
FileSystem::~FileSystem()
{

}


/**
 * @brief Create a File System instance or Provide if exist already
 * 
 * @return std::shared_ptr<FileSystem> FileSystem class singleton object
 */
std::shared_ptr<FileSystem> FileSystem::getInstance()
{
    if (instance_ == nullptr)
    {
        instance_ = std::shared_ptr<FileSystem>(new FileSystem());
    }
    return instance_;
}


/**
 * @brief Function creats a new directory for given name at given directory level.
 * 
 * @param directoryName : directiry to be created
 * @param parent : at which directory level, directory should be created
 * @return true if creation successful
 * @return false if directory already exist
 */
bool FileSystem::createDirectory(std::string directoryName, std::shared_ptr<FileSystemComponentInterface> parent)
{
    /* search for exiting */

    /* create new instance of file system component */
    auto newDirectory = std::shared_ptr<FileSystemComponent>(new FileSystemComponent());
    newDirectory->setName(directoryName);
    newDirectory->setSize(0);
    newDirectory->setFlags(1);

    return parent->insertChildNode(newDirectory);
}


/**
 * @brief List down the sub directories and files under given directory level
 * 
 * @param parent given directory level
 * @return std::vector<std::string> : list of directories and files
 */
std::vector<std::string> FileSystem::listDirectory(std::shared_ptr<FileSystemComponentInterface> parent)
{
    return parent->listChildNodes();
}

