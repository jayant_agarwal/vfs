/**
 * @file FileSystemComponent.cpp
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2020-04-02
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#include <string>
#include <iostream>
#include <vector>
#include <sstream>
#include <iomanip>
#include <cinttypes>

#include "FileSystemComponent.h"
#include "FileComponentKey.h"

/**
 * @brief Construct a new File System Component:: File System Component object
 * 
 */
FileSystemComponent::FileSystemComponent()
{

}

/**
 * @brief Destroy the File System Component:: File System Component object
 * 
 */
FileSystemComponent::~FileSystemComponent()
{

}

/**
 * @brief Function fetches size of Directory/File
 * 
 * @return int size of Directory/File
 */
int FileSystemComponent::getSize()
{
    return size_;    
}

/**
 * @brief Stores size of Directory/File
 * 
 * @param size of Directory/File
 */
void FileSystemComponent::setSize(int size)
{
      size_ = size;  
}


/**
 * @brief Function fetches name of Directory/File
 * 
 * @return std::string name of Directory/File
 */
std::string FileSystemComponent::getName()
{
    return name_;    
}

/**
 * @brief Stores name of Directory/File
 * 
 * @param name of Directory/File
 */
void FileSystemComponent::setName(std::string name)
{
      name_ = name;  
}


/**
 * @brief Inserts given node into the unordered map 
 * 
 * @param child given node
 * @return true if node insertion is successful
 * @return false if node already exist
 */
bool FileSystemComponent::insertChildNode(std::shared_ptr<FileSystemComponentInterface> child)
{
    bool isDirectory = child->getFlags() == 1;
    FileComponentKey key(child->getName(), isDirectory);
    auto result = subComponents_.emplace(std::make_pair(key, child));
    return result.second;
}


/**
 * @brief Function fetches all subnodes of current component and 
 * convert that to display format
 * 
 * @return std::vector<std::string> list of files and directories in display format
 */
std::vector<std::string> FileSystemComponent::listChildNodes()
{
    std::vector<std::string> list;
    auto it = subComponents_.begin();
    while(it != subComponents_.end())
    {
        std::stringstream ss;
        auto node = it->second;
        ss << std::setw(30) << std::left << node->getName();
        ss << std::setw(5) << std::left << " ";
        ss << std::setw(4) << std::internal <<  (node->getFlags() == 1 ? "dir" : "file");
        ss << std::setw(5) << std::left << " ";
        ss << std::setw(10) << std::left << node->getSize();
        list.push_back(ss.str());
        it++;
    }

    return list;
}


/**
 * @brief Get the Flags of Directory/File
 * 
 * @return uint16_t Flags of Directory/File
 */
uint16_t FileSystemComponent::getFlags()
{
    return flags_;
}


/**
 * @brief Set the Flags of Directory/File
 * 
 * @param flags Flags of Directory/File
 */
void FileSystemComponent::setFlags(uint16_t flags)
{
    flags_ = flags;
}